package com.alexmt.algopractice.datastructures;

import org.junit.Assert;
import org.junit.Test;

public class TreeNodeTests {
    @Test()
    public void inOrder() {
        TreeNode<Integer> tree = new TreeNode<>(4,
                new TreeNode<>(2,
                        new TreeNode<>(1, null, null),
                        new TreeNode<>(3, null, null)),
                new TreeNode<>(5,
                        null,
                        new TreeNode<>(6, null, null)));
        final StringBuilder result = new StringBuilder();
        tree.traverseInOrder(node -> result.append(node.val));
        Assert.assertEquals("123456", result.toString());
    }
}
