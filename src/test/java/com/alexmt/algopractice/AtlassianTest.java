package com.alexmt.algopractice;

import org.junit.Assert;
import org.junit.Test;

public class AtlassianTest {

    @Test
    public void kmpSearchTest() {
        Assert.assertArrayEquals(
            Atlassian.kmpSearch("AAAAABAAABA", "AAAA").toArray(),
            new Object[] {0, 1});
    }

    @Test
    public void minWindowTest() {
        Assert.assertEquals("BANC", Atlassian.minWindow("ADOBECODEBANC", "ABC"));
    }
}
