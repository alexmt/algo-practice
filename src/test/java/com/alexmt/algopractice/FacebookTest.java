package com.alexmt.algopractice;

import org.junit.Assert;
import org.junit.Test;

public class FacebookTest {

    @Test
    public void getSortedSquaredArray_sortsArrayWithPositiveAndNegative() {
        Assert.assertArrayEquals(
                new int[] { 0, 1, 4, 4, 9 },
                Facebook.getSortedSquaredArray(new int[] { -2, -1, 0, 2, 3 }));
    }

    @Test
    public void getNumberOfPathsTest() {
        Assert.assertEquals(2, Facebook.getNumberOfPaths(1,1, 2, 5, 5));
        Assert.assertEquals(1, Facebook.getNumberOfPaths(0,1, 1, 5, 5));
    }
}
