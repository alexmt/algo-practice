package com.alexmt.algopractice;

import org.junit.Assert;
import org.junit.Test;

public class UberTest {

    @Test
    public void maximumSumOfNonAdjacentTest() {
        Assert.assertEquals(10, Uber.maximumSumOfNonAdjacent(new int[] {1, 0, 3, 9, 2}));
        Assert.assertEquals(18, Uber.maximumSumOfNonAdjacent(new int[] {2, 9, 3, 9, 2}));
    }
}
