package com.alexmt.algopractice;

import com.alexmt.algopractice.datastructures.TreeNode;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

public class AmazonTest {

    @Test()
    public void findAnagramIndices() {
        Assert.assertArrayEquals(new Object[]{0, 4, 8}, Amazon.findAnagramIndices("AB", "ABCDBACDAB").toArray());
    }

    @Test()
    public void findLargestSubSquare() {
        Assert.assertEquals(2, Amazon.findLargestSubSquare(new int[][]{
                        new int[]{0, 1, 1, 1},
                        new int[]{1, 1, 1, 0},
                        new int[]{1, 1, 1, 0},
                        new int[]{0, 0, 0, 0},
                        new int[]{0, 1, 1, 0}
        }));
    }

    @Test()
    public void findThirdHighestValue() {
        TreeNode<Integer> tree = new TreeNode<>(4,
                new TreeNode<>(2,
                        new TreeNode<>(1, null, null),
                        new TreeNode<>(3, null, null)),
                new TreeNode<>(6,
                        null,
                        null));
        Assert.assertEquals(3, Amazon.findThirdHighestValue(tree));
    }

    @Test
     public void wordLadder() {
        Amazon.wordLadder("hit", "cog", Arrays.asList("hot","dot","dog","lot","log","cog"));
    }
}
