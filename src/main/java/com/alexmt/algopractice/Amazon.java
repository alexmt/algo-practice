package com.alexmt.algopractice;

import com.alexmt.algopractice.datastructures.TreeNode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.stream.Collectors;

/**
 * https://www.careercup.com/page?pid=amazon-interview-questions
 */
public class Amazon {

    /**
     * Find the indices of all anagrams of a given word in a another word.
     * For example: Find the indices of all the anagrams of AB in ABCDBACDAB (Answer: 0, 4, 8)
     */
    public static List<Integer> findAnagramIndices(String target, String src) {
        List<Integer> result = new ArrayList<>();
        if (src.length() < target.length()) {
            return result;
        }

        int targetHash = 0;
        int hash = 0;
        for (int i = 0; i < target.length() - 1; i++) {
            targetHash += target.charAt(i);
            hash += src.charAt(i);
        }
        targetHash += target.charAt(target.length() - 1);

        for (int i = target.length() - 1; i < src.length(); i++) {
            hash += src.charAt(i);
            if (targetHash == hash) {
                result.add(i - target.length() + 1);
            }
            hash -= src.charAt(i - 1);
        }
        return result;
    }


    /**
     * Given MxN binary matrix, find the largest sub-square matrix with all 1’s.
     *
     * 0 1 1 1
     * 1 1 1 0
     * 1 1 1 0
     * 0 0 0 0
     * 0 1 1 0
     */
    public static int findLargestSubSquare(int[][] input) {
        for (int i = 1; i < input.length; i++) {
            for (int j = 1; j < input[0].length; j++) {
                if (input[i][j] > 0) {
                    input[i][j] = Math.min(Math.min(input[i - 1][j], input[i - 1][j - 1]), input[i][j - 1]) + 1;
                }
            }
        }
        int max = 0;
        for (int i = 1; i < input.length; i++) {
            for (int j = 1; j < input[0].length; j++) {
                max = Math.max(input[i][j], max);
            }
        }
        return max;
    }

    /**
     * Find third highest value in a binary tree
     * 4
     * |\
     * 2  6
     * |\
     * 1 3
     */
    public static int findThirdHighestValue(TreeNode<Integer> root) {
        List<Integer> values = new LinkedList<>();
        iterateInOrder(root, values);
        if (values.size() > 2) {
            return values.get(2);
        }
        return -1;
    }

    private static void iterateInOrder(TreeNode<Integer> node, List<Integer> values) {
        if (node.left != null) {
            iterateInOrder(node.left, values);
        }
        values.add(node.val);
        if (node.right != null) {
            iterateInOrder(node.right, values);
        }
    }

    private static boolean hasOneLetterDifference(String first, String second) {
        int diffCount = 0;
        for (int i = 0; i < first.length(); i++) {
            if (first.charAt(i) != second.charAt(i)) {
                diffCount++;
            }
            if (diffCount > 1) {
                return false;
            }
        }
        return diffCount == 1;
    }

    public static List<List<String>> wordLadder(String beginWord, String endWord, List<String> wordList) {
        List<List<String>> result = new ArrayList<>();
        Map<String, List<String>> edges = new HashMap<>();
        edges.put(beginWord,
                wordList.stream().filter(item -> hasOneLetterDifference(beginWord, item)).collect(Collectors.toList()));
        for (String word : wordList) {
            edges.put(word,
                    wordList.stream().filter(item -> hasOneLetterDifference(word, item)).collect(Collectors.toList()));
        }

        Queue<List<String>> queue = new ArrayDeque<>();
        queue.add(new ArrayList<>(Arrays.asList(beginWord)));
        while (!queue.isEmpty()) {
            List<String> next = queue.poll();
            if (next.get(next.size() - 1).equals(endWord)) {
                if (result.size() > 0 && result.get(0).size() < next.size()) {
                    break;
                } else {
                    result.add(next);
                }
            } else {
                for (String word : edges.get(next.get(next.size() - 1))) {
                    List<String> child = new ArrayList<>(next);
                    child.add(word);
                    queue.add(child);
                }
            }
        }
        return result;
    }
}
