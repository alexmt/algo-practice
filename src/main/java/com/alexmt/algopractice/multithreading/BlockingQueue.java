package com.alexmt.algopractice.multithreading;

import java.util.ArrayDeque;
import java.util.Queue;

public class BlockingQueue<T> {

    private Queue<T> items = new ArrayDeque<>();

    public void add(T item) {
        synchronized (items) {
            this.items.add(item);
            this.notifyAll();
        }
    }

    public T take() throws InterruptedException {
        synchronized (this.items) {
            while (!items.isEmpty()) {
                this.wait();
            }
            return items.poll();
        }
    }
}
