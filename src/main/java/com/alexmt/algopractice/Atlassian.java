package com.alexmt.algopractice;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Atlassian {

    public static String minWindow(String input, String target) {
        String result = null;
        Map<Character, Integer> t = new HashMap<>();
        for (int i = 0; i < target.length(); i++) {
            char next = target.charAt(i);
            if (t.containsKey(next)) {
                t.put(next, t.get(next) + 1);
            } else {
                t.put(next, 1);
            }
        }
        int left = 0;
        int foundCnt = 0;
        Map<Character, Integer> found = new HashMap<>();

        for (int i = 0; i < input.length(); i++) {
            char next = input.charAt(i);
            if (t.containsKey(next)) {
                if (!found.containsKey(next)) {
                    found.put(next, 1);
                } else {
                    found.put(next, found.get(next) + 1);
                }
                if (t.get(next) == found.get(next)) {
                    foundCnt++;
                }
            }

            if (foundCnt == t.keySet().size()) {
                char leftChar = input.charAt(left);
                while (found.containsKey(leftChar) && t.get(leftChar) < found.get(leftChar) || !found.containsKey(leftChar)) {
                    if (found.containsKey(leftChar)) {
                        found.put(leftChar, found.get(leftChar) - 1);
                    }
                    left++;
                    leftChar = input.charAt(left);
                }
                if (result == null || result.length() > i - left) {
                    result = input.substring(left, i + 1);
                }
            }
        }

        return result;
    }

    private static int[] getFailureArray(String pattern) {
        int[] result = new int[pattern.length()];
        result[0] = 0;
        int i = 0;
        int j = 1;
        while (j < pattern.length()) {
            if (pattern.charAt(i) == pattern.charAt(j)) {
                result[j] = result[i] + 1;
                i++;
                j++;
            } else {
                if (i > 0 && i != result[i - 1]) {
                    i = result[i - 1];
                } else {
                    j++;
                }
            }
        }
        return result;
    }

    public static List<Integer> kmpSearch(String input, String pattern) {
        if (pattern.length() == 0) {
            return Arrays.asList();
        }
        if (input.length() < pattern.length()) {
            return Arrays.asList();
        }
        List<Integer> result = new LinkedList<>();
        int[] failureArray = getFailureArray(pattern);
        int i = 0;
        while (i < input.length() - pattern.length()) {
            for (int j = 0; j < pattern.length(); j++) {
                if (pattern.charAt(j) != input.charAt(i + j)) {
                    i += failureArray[j];
                    break;
                } else {
                    if (j == pattern.length() - 1) {
                        result.add(i);
                    }
                }
            }
            i++;
        }

        return result;
    }
}
