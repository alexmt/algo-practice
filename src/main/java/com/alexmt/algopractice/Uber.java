package com.alexmt.algopractice;

public class Uber {
    private static int maximumSumOfNonAdjacent(int[] input, int pos) {
        int max = 0;
        if (pos >= input.length - 1) {
            max = input[pos];
        } else {
            for (int i = pos + 2; i < input.length; i++) {
                int next = maximumSumOfNonAdjacent(input, i);
                if (next > max) {
                    max = next;
                }
            }
        }
        return input[pos] + max;
    }

    /**
     * http://blog.gainlo.co/index.php/2016/12/02/uber-interview-question-maximum-sum-non-adjacent-elements/
     * Given an array of integers, find a maximum sum of non-adjacent elements.
     * For example, inputs [1, 0, 3, 9, 2] should return 10 (1 + 9).
     */
    public static int maximumSumOfNonAdjacent(int[] input) {
        int max = 0;
        for (int i = 0; i < input.length - 1; i++) {
            int next = maximumSumOfNonAdjacent(input, i);
            if (next > max) {
                max = next;
            }
        }
        return max;
    }
}
