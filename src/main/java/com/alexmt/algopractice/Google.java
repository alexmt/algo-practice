package com.alexmt.algopractice;

import java.util.ArrayDeque;
import java.util.Arrays;

public class Google {

    public static void main(String[] args) {
        int dist = ballInMaze(new int[][] {
            { 0, 0, 1, 0, 0 },
            { 0, 0, 0, 0, 0 },
            { 0, 0, 0, 1, 0 },
            { 1, 1, 0, 1, 1 },
            { 0, 0, 0, 0, 0 },
        }, new int[] {0, 4}, new int[] {4, 4});
        System.out.println(dist);
    }

    public static int ballInMaze(int[][] maze, int[] start, int[] dst) {
        int dist[][] = new int[maze.length][maze[0].length];
        for (int[] row : dist) {
            Arrays.fill(row, Integer.MAX_VALUE);
        }
        ArrayDeque<int[]> queue = new ArrayDeque<>();
        queue.add(start);
        while (!queue.isEmpty()) {
            int[] s = queue.poll();
        }
        return dist[dst[0]][dst[1]] == Integer.MAX_VALUE ? -1 : dist[dst[0]][dst[1]];
    }
   
}
