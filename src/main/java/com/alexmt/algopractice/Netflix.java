package com.alexmt.algopractice;

import java.util.Arrays;

public class Netflix {

    public static int countPolySubstrings(String s) {
        int[] counts = new int[s.length() + 1];
        counts[1] = 1;
        for (int i = 2; i <= s.length(); i++) {
            String subStr = s.substring(0, i);
            if (new StringBuffer(subStr).reverse().toString().equals(subStr)) {
                counts[i] = counts[i - 1] + 2;
            } else {
                counts[i] = counts[i - 1] + 1;
            }
        }
        return counts[s.length()];
    }

    public static int leastNumberOfCoins(int[] coins, int amount) {
        int max = amount + 1;
        int[] dp = new int[max];
        Arrays.fill(dp, max);
        dp[0] = 0;
        for (int i = 1; i <= amount; i++) {
            for (int j = 0; j < coins.length; j++) {
                if (coins[j] <= i) {
                    dp[i] = Math.min(dp[i], dp[i - coins[j]] + 1);
                }
            }
        }
        return dp[amount] > amount ? -1 : dp[amount];
    }
}
