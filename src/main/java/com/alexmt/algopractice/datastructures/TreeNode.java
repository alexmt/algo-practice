package com.alexmt.algopractice.datastructures;

import java.util.Deque;
import java.util.LinkedList;
import java.util.function.Consumer;

public class TreeNode<T> {

    public T val;
    public TreeNode<T> left;
    public TreeNode<T> right;

    @Override
    public String toString() {
        return val.toString();
    }

    public TreeNode(T val, TreeNode<T> left, TreeNode<T> right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }

    public void traverseInOrder(Consumer<TreeNode<T>> action) {
        Deque<TreeNode<T>> nodes = new LinkedList<>();
        nodes.push(this);
        TreeNode<T> node = this.left;
        while(node != null) {
            nodes.push(node);
            node = node.left;
        }
        while (!nodes.isEmpty()) {
            node = nodes.pop();
            action.accept(node);

            node = node.right;
            while (node != null) {
                while(node != null) {
                    nodes.push(node);
                    node = node.left;
                }
            }
        }
    }
}
