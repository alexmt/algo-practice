package com.alexmt.algopractice;

/**
 * https://www.careercup.com/page?pid=facebook-interview-questions
 */
public class Facebook {
    /**
     * Given a sorted list of integers, square the elements and give the output in sorted order.
     */
    public static int[] getSortedSquaredArray(int[] input) {
        int[] result = new int[input.length];
        int left = 0;
        int right = input.length - 1;
        int i = input.length - 1;
        while (left < right) {
            if (Math.abs(input[left]) < Math.abs(input[right])) {
                result[i] = input[right] * input[right];
                right--;
            } else {
                result[i] = input[left] * input[left];
                left++;
            }
            i--;
        }
        return result;
    }


    /**
     * Given a matrix of N * M, given the coordinates (x, y) present in a matrix,
     * Find the number of paths that can reach (0, 0) from the (x, y) points with k steps (requires exactly k, k> = 0)
     * From each point you can go up, down, left and right in four directions.
     * For example, the following matrix:
     *
     * 0 0 0 0 0
     * 0 0 0 0 0
     * 0 0 0 0 0
     * 0 0 0 0 0
     *
     * (x, y) = (1, 1), k = 2, the output is 2
     */
    public static int getNumberOfPaths(int x, int y, int k, int n, int m) {
        if (k == 0 && x == 0 && y == 0) {
            return 1;
        } else if (k == 0) {
            return 0;
        }

        int result = 0;
        if (x > 0) {
            result = result + getNumberOfPaths(x - 1, y, k - 1, n, m);
        }
        if (y > 0) {
            result = result + getNumberOfPaths(x, y - 1, k - 1, n, m);
        }
        if (x < n) {
            result = result + getNumberOfPaths(x + 1, y, k - 1, n, m);
        }
        if (y < m) {
            result = result + getNumberOfPaths(x, y + 1, k - 1, n, m);
        }
        return result;
    }
}
